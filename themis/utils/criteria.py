from zope.interface import implements
from Products.CMFCore.permissions import View
from Products.CMFCore.utils import getToolByName
from AccessControl import ClassSecurityInfo

from Products.Archetypes.atapi import Schema
from Products.Archetypes.atapi import StringField
from Products.Archetypes.atapi import StringWidget
from Products.Archetypes.atapi import registerType

from Products.ATContentTypes.criteria import _criterionRegistry

from Products.ATContentTypes.criteria import ALL_INDICES
from Products.ATContentTypes.permission import ChangeTopics
from Products.ATContentTypes.criteria.base import ATBaseCriterion
from Products.ATContentTypes.criteria.schemata import ATBaseCriterionSchema
from Products.ATContentTypes.interfaces import IATTopicSearchCriterion

from zope.i18nmessageid import MessageFactory

from themis.utils import PROJECTNAME

from themis.utils.config import ZOPE2_VERSION

_ = MessageFactory('themis.utils')

CatalogCriterionSchema = ATBaseCriterionSchema + Schema((

    StringField('key',
                required=1,
                mode="rw",
                write_permission=ChangeTopics,
                accessor="Key",
                mutator="setKey",
                default="",
                widget=StringWidget(
                    label=_(u'label_criteria_key', default=u'Catalog Key'),
                    description=_(u'help_criteria_key', default=u'Catalog Key to match to content')
                    ),
                ),

    StringField('value',
                required=1,
                mode="rw",
                write_permission=ChangeTopics,
                accessor="Value",
                mutator="setValue",
                default="",
                widget=StringWidget(
                    label=_(u'label_criteria_value', default=u'Catalog Value'),
                    description=_(u'help_criteria_value', default=u'Catalog Value to match to content')
                    ),
                ),

    ))

class CatalogCriterion(ATBaseCriterion):
    if ZOPE2_VERSION >= 2.12:
        implements(IATTopicSearchCriterion)
    else:
        __implements__ = ATBaseCriterion.__implements__ + (IATTopicSearchCriterion, )

    security       = ClassSecurityInfo()
    schema         = CatalogCriterionSchema
    meta_type      = 'CatalogCriterion'
    archetype_name = 'Catalog criterion'
    shortDesc      = 'Catalog property'

    security.declareProtected(View, 'getCriteriaItems')
    def getCriteriaItems(self):
        field = self.Field()
        key_name = self.Key()
        value_name = self.Value()
        if value_name == 'True':
            value_name = True
        elif value_name == 'False':
            value_name = False
        return ((key_name, value_name),)


def register(criterion, indices):
    if isinstance(indices, basestring):
        indices = (indices,)
    indices = tuple(indices)

    if indices == ():
        indices = ALL_INDICES

    registerType(criterion, PROJECTNAME)

    crit_id = criterion.meta_type
    _criterionRegistry[crit_id] = criterion
    _criterionRegistry.portaltypes[criterion.portal_type] = criterion

    _criterionRegistry.criterion2index[crit_id] = indices
    for index in indices:
        value = _criterionRegistry.index2criterion.get(index, ())
        _criterionRegistry.index2criterion[index] = value + (crit_id,)

register(CatalogCriterion, ALL_INDICES)
